# Unity Voxel Based Buoyancy #

This is a fork of the original script written by [Alex Zhdankin](http://forum.unity3d.com/threads/buoyancy-script.72974/)

Originally licensed under [WTFPL version 2](http://www.wtfpl.net/about/)

### Steps to get working in a project ###

1. Add Buoyancy.cs to project
2. Add a rigidbody and collider(s) to object
3. Add Buoyancy component to object
4. Set up Buoyancy component parameters
5. Float!
 
### Water Systems Compatibility ###

* Ceto (planned)